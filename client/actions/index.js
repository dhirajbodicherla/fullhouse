import * as types from '../constants/ActionTypes'
import axios from 'axios'

const requestAddPost = () => ({
	type: types.REQUEST_ADD_POST
})

const receiveAddPost = (payload) => ({
	type: types.RECEIVE_ADD_POST,
	payload
})

const addPostError = () => ({
	type: types.ADD_POST_ERROR
})

const requestPosts = () => ({
	type: types.REQUEST_POSTS
})

const receivePosts = (payload) => ({
	type: types.RECEIVE_POSTS,
	payload
})

const receivePostsError = () => ({
	type: types.RECEIVE_POSTS_ERROR
})

const requestFamilySentiment = () => ({
	type: types.REQUEST_FAMILY_SENTIMENT
})

const receiveFamilySentiment = (payload) => ({
	type: types.RECEIVE_FAMILY_SENTIMENT,
	payload
})

const getFamilySentimentError = () => ({
	type: types.RECEIVE_FAMILY_SENTIMENT_ERROR
})

export function getPosts(userId) {
	return dispatch => {
		dispatch(requestPosts())
		return axios.get('/post')
			.then(response => {
				dispatch(receivePosts(response.data))
			})
			.catch(err => {
				console.log(err)
				dispatch(receivePostsError())
			})
	}
}

export function addPost(post) {
	return dispatch => {
		dispatch(requestAddPost())
		return axios
			.post('/post', post)
			.then(response => {
				dispatch(receiveAddPost(response.data))
			})
			.catch(err => {
				console.log(err)
				dispatch(addPostError())
			})
	}
}

export function getFamilySentiment() {
	return dispatch => {
		dispatch(requestFamilySentiment())
		return axios
			.get('/familysentiment')
			.then(response => {
				dispatch(receiveFamilySentiment(response.data))
			})
			.catch(err => {
				dispatch(getFamilySentimentError(err))
			})
	}
}

export function updateFamilySentiment(payload) {
	return dispatch => {
		dispatch(receiveFamilySentiment(payload))
	}
}