import React, { Component } from 'react'
import io from 'socket.io-client'
import { connect } from 'react-redux'
import { getFamilySentiment, updateFamilySentiment } from '../actions'
import { SOCKET_URL } from '../constants/App'

@connect((store) => {
	return store.sentiment
})
export default class Family extends Component {
	componentDidMount() {
		this.socket = io.connect(SOCKET_URL)
		this.socket.on('connect', this.onSocketConnect.bind(this))
		this.socket.on('message', this.onFamilySentimentUpdate.bind(this))
		this.props.dispatch(getFamilySentiment())
	}
	componentWillUnmount() {
		this.socket.emit('leaveroom', this.props.user.family);
	}
	onFamilySentimentUpdate(sentiment) {
		let newSentiment = {
			count: this.props.sentiment.count,
			average_sentiment: this.props.sentiment.average_sentiment
		};
		let updatedSentiment = newSentiment.count * newSentiment.average_sentiment + sentiment
		newSentiment.count = newSentiment.count + 1
		newSentiment.average_sentiment = (updatedSentiment / newSentiment.count).toFixed(2)
		this.props.dispatch(updateFamilySentiment(newSentiment));
	}
	onSocketConnect() {
		this.socket.emit('joinroom', this.props.user.family);
	}
	render() {
		const sentiment = this.props.sentiment.average_sentiment
		return <div className='familysentiment-container'>
			<div>
				<p className='familysentiment-value'>{sentiment}</p>
				<p className='familysentiment-label'>Family Sentiment</p>
			</div>
		</div>
	}
}