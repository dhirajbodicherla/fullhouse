import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import AppBar from 'material-ui/AppBar'
import MenuItem from 'material-ui/MenuItem'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import FontIcon from 'material-ui/FontIcon'
import { white500 } from 'material-ui/styles/colors'
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'
import FlatButton from 'material-ui/FlatButton'

function HomeIcon(props) {
	const style = {
		top: 10,
		position: 'relative',
		fontSize: 30,
		color: 'white'
	};
	return <FlatButton
		icon={<i className='material-icons'>home</i>}
		style={style}
		onTouchTap={props.onHomeIconTap}
    />
}

function FamilyIcon(props) {
	return (
		<FlatButton
			style={{top: -5, color: 'white'}}
			label="Family Sentiment"
			onTouchTap={props.onFamilyIconTap}
		/>
	)
}

function Settings(props) {
	return <div>
		{props.isFamilyHead === 1 &&
			<FamilyIcon
				{...props}
				onFamilyIconTap={props.onFamilyIconTap}
			/>
		}
		<IconMenu
			iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
			targetOrigin={{horizontal: 'right', vertical: 'top'}}
			anchorOrigin={{horizontal: 'right', vertical: 'top'}}
			onItemTouchTap={props.onItemTouch}
			iconStyle={{color: 'white'}}
		>
			<MenuItem primaryText='Help' value='1'/>
			<MenuItem primaryText='Sign out' value='2'/>
		</IconMenu>
	</div>
}

export default class Header extends Component {
	constructor(props) {
		super(props)
		this.onHomeIconTap = this.onHomeIconTap.bind(this)
		this.onItemTouch = this.onItemTouch.bind(this)
		this.onFamilyIconTap = this.onFamilyIconTap.bind(this)
	}
	onHomeIconTap() {
		this.props.history.push('/');
	}
	onFamilyIconTap() {
		this.props.history.push('/family');
	}
	onItemTouch(e, child) {
		if (child.props.value === '2') {
			window.location = '/logout';
		} else if (child.props.value === '0') {
			this.props.history.push('/family');
		}
	}
	render() {
		const title = 'Welcome to ' + this.props.user.family + '\'s family Journal'
		return <AppBar
			titleStyle={{textAlign: 'center'}}
			title={title}
			iconElementLeft={<HomeIcon onHomeIconTap={this.onHomeIconTap}/>}
			iconElementRight={<Settings
				onItemTouch={this.onItemTouch}
				isFamilyHead={this.props.user.familyhead}
				onFamilyIconTap={this.onFamilyIconTap}
			/>}
		/>
	}
}