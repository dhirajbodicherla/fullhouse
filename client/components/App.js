import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Header from './Header'
import Journal from './Journal'
import Family from './Family'

export default class App extends Component {
	render() {
		return (
			<Router>
				<div>
					<Route path="*" render={(props) => <Header user={this.props.user} {...props}/>} />
					<Route exact path="/" component={() => <Journal user={this.props.user} />} />
					<Route path="/home" component={() => <Journal user={this.props.user} />} />
					<Route path="/family" component={(props) => <Family user={this.props.user} />} />
				</div>
			</Router>
		)
	}
}