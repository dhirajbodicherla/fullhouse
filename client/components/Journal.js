import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { addPost } from '../actions'
import { connect } from 'react-redux'
import Button from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem from 'material-ui/MenuItem'
import {Card, CardHeader, CardText} from 'material-ui/Card'

function PostRow(props) {
	const cardStyle = {
		position: 'relative',
		marginBottom: 2
	};
	const cardHeaderStyle = {
		paddingTop: 10,
		paddingRight: 10,
		paddingBottom: 0,
		paddingLeft: 10
	};
	const cardHeaderTitleStyle = {
		fontWeight: 'bold'
	};
	const cardTextStyle = {
		fontSize: 16,
		paddingBottom: 5
	}
	const happinessStyle = {
		position: 'absolute',
		top: 0,
		fontSize: 14,
		left: 30,
		transform: 'rotate(-20deg)',
		fontWeight: 'bold',
		backgroundColor: 'rgb(236, 167, 40)',
		border: '1px solid rgb(220, 157, 40)',
		color: 'white',
		borderRadius: 16,
		width: 17,
		textAlign: 'center'
	};
	return (
		<Card expandable={false} style={cardStyle}>
			<CardHeader
				title={props.user.username}
				titleStyle={cardHeaderTitleStyle}
				avatar={<i className="material-icons">face</i>}
				style={cardHeaderStyle}
			/>
			<span style={happinessStyle}>{props.data.happiness}</span>
			<CardText style={cardTextStyle}>
				{props.data.post}
			</CardText>
		</Card>
	)
}

@connect((store) => {
	return store.posts
})
export default class Journal extends Component {
	constructor(props) {
		super(props)
		this.state = {
			postText: '',
			postHappiness: 1
		}
		this.onPostSubmit = this.onPostSubmit.bind(this)
		this.updatePostText = this.updatePostText.bind(this)
		this.updateHappiness = this.updateHappiness.bind(this)
	}
	onPostSubmit() {
		if (this.state.postText.trim() === '') {
			return;
		}
		this.setState({
			postText: ''
		})
		this.props.dispatch(addPost({
			post: this.state.postText,
			happiness: this.state.postHappiness
		}))
	}
	updatePostText(e) {
		this.setState({
			postText: e.target.value
		})
	}
	updateHappiness(e, index, value) {
		this.setState({
			postHappiness: value
		})
	}
	render() {
		const items = this.props.posts.map((post) => <PostRow key={post._id} data={post} {...this.props}/>)
		const gridStyle = {
			marginLeft: 15,
			marginRight: 15
		};
		const actionsContainerStyle = {
			float: 'right'
		};
		const happinessTextContainerStyle = {
			fontSize: 18
		};
		const happinessIconStyle = {
			position: 'relative',
			top: 5
		};
		const dropdownContainerStyle = {
			top: -11,
			position: 'relative'
		};
		const postSubmitButtonStyle = {
			color: 'white'
		};

		let happinessItems = [];
		for (var i = -10; i <= 10; i++) {
			// Have to convert the primaryText to a string otherwise 0 will not print
			// bug with MenuItem component
			happinessItems.push(<MenuItem key={i} value={i} primaryText={i + ''} />)
		}

		return <Grid style={gridStyle} fluid>
			<Row>
				<Col md={12}>
					<TextField
						floatingLabelText='How are things ?'
						multiLine={true}
						fullWidth={true}
						rows={2}
						value={this.state.postText}
						onChange={this.updatePostText}
					/>
				</Col>
			</Row>
			<Row end="md">
				<Col md={3} style={happinessTextContainerStyle}>
					<i
						className='material-icons'
						style={happinessIconStyle}
					>
						ic_sentiment_very_satisfied
					</i> How happy are you ?
				</Col>
				<Col md={1} style={dropdownContainerStyle}>
					<DropDownMenu
						value={this.state.postHappiness}
						onChange={this.updateHappiness}
						autoWidth={true}
					>
						{happinessItems}
					</DropDownMenu>
				</Col>
				<Col md={1}>
					<Button
						primary={true}
						onClick={this.onPostSubmit}
						buttonStyle={postSubmitButtonStyle}
					>
						Post
					</Button>
				</Col>
			</Row>
			<Row>
				<Col md={12}>
					{items}
				</Col>
			</Row>
		</Grid>
	}
}