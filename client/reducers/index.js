import { combineReducers } from 'redux'
import posts from './posts'
import sentiment from './sentiment'

const reducers = combineReducers({
	posts: posts,
	sentiment: sentiment
})

export default reducers