import { REQUEST_ADD_POST, RECEIVE_ADD_POST, REQUEST_POSTS, RECEIVE_POSTS } from '../constants/ActionTypes'

const initState = {
	posts: [],
	postsFetching: false,
	postsFetched: false,
	postAdding: false,
	postAdded: false
}

const postsReducer = (state = initState, action) => {
	switch (action.type) {
		case REQUEST_ADD_POST: {
			return {
				...state,
				postsFetching: false,
				postsFetched: false,
				postAdding: true,
				postAdded: false
			}
		}
		case RECEIVE_ADD_POST: {
			return {
				...state,
				postsFetching: false,
				postsFetched: false,
				postAdding: true,
				postAdded: false,
				posts: [action.payload, ...state.posts]
			}
		}
		case REQUEST_POSTS: {
			return {
				...state,
				postsFetching: true,
				postsFetched: false,
				postAdding: false,
				postAdded: false
			}
		}
		case RECEIVE_POSTS: {
			return {
				...state,
				postsFetching: false,
				postsFetched: true,
				postAdding: false,
				postAdded: false,
				posts: action.payload
			}
		}
	}
	return state
}

export default postsReducer