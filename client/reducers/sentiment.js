import { REQUEST_FAMILY_SENTIMENT, RECEIVE_FAMILY_SENTIMENT, RECEIVE_FAMILY_SENTIMENT_ERROR } from '../constants/ActionTypes'

const initState = {
	sentiment: {
		count: 0,
		average_sentiment: 0
	},
	sentimentFetching: false,
	sentimentFetched: false
}

const sentimentReducer = (state = initState, action) => {
	switch (action.type) {
		case REQUEST_FAMILY_SENTIMENT: {
			return {
				...state,
				sentimentFetching: true,
				sentimentFetched: false
			}
		}
		case RECEIVE_FAMILY_SENTIMENT: {
			return {
				...state,
				sentimentFetching: false,
				sentimentFetched: false,
				sentiment: action.payload
			}
		}
	}
	return state
}

export default sentimentReducer