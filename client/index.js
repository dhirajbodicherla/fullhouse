import React from 'react'
import { render } from 'react-dom'
import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import injectTapEventPlugin from 'react-tap-event-plugin'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import { getPosts } from './actions'
import reducers from './reducers'
import App from './components/App'

const mountNode = document.getElementById('app')
const user = JSON.parse(mountNode.getAttribute('data-user'))
const store = createStore(reducers, applyMiddleware(thunk))

store.dispatch((dispatch) => {
	dispatch(getPosts())
})

injectTapEventPlugin();

render(
	<Provider store={store}>
		<MuiThemeProvider>
			<App user={user}/>
		</MuiThemeProvider>
	</Provider>,
	mountNode
)