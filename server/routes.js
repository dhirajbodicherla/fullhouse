const Post = require('./models/Post')
const User = require('./models/User')
const SOCKET_PORT = process.env.SOCKET_PORT || 3000
const VIEW_DIR = '/views/'
const SOCKET_URL = 'http://localhost:' + SOCKET_PORT + '/socket.io/socket.io.js'

module.exports = (app, passport, SocketBus) => {
	/**
	 *
	 * Route default, home
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 */
	app.get(['/', '/home'], isLoggedIn, (req, res) => {
		const user = req.user
		const viewPayload = {
			user : JSON.stringify(user),
			socket_url: (user.familyhead ? SOCKET_URL : '')
		}
		res.render(__dirname + VIEW_DIR + 'home.ejs', viewPayload)
	})
	/**
	 *
	 * Route /family
	 * This is special because we do not want to
	 * allow non-family heads to see this route
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 */
	app.get('/family', isUserAuthorized, (req, res) => {
		const user = req.user
		const viewPayload = {
			user : JSON.stringify(user),
			socket_url: (user.familyhead ? SOCKET_URL : '')
		}
		res.render(__dirname + VIEW_DIR + 'home.ejs', viewPayload)
	})
	/**
	 *
	 * Route to get family sentiment details
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 * @param  {Function} next - Next middleware function
	 */
	app.get('/familysentiment', isUserAuthorized, (req, res, next) => {
		User.find({family: req.user.family}, '_id')
			.then((response, err) => {
				if (err) {
					return next('Error fetching sentiment score. Please try again')
				}
				const ids = response.map(item => item._id)
				return Post.
					find({ userId: { $in : ids }})
			})
			.then((response, err) => {
				if (err) {
					return next('Error fetching sentiment score. Please try again')
				}

				const totalSentiment = response.reduce((a, b) => ({sentiment: a.sentiment + b.sentiment})).sentiment;
				const returnValue = {}
				returnValue.count = response.length
				returnValue.average_sentiment = (totalSentiment / response.length).toFixed(2)
				res.setHeader('Content-Type', 'application/json')
				res.send(JSON.stringify(returnValue))
			})
			.catch(function(err) {
				return next('Error fetching sentiment score. Please try again')
			})
	})
	/**
	 *
	 * Route get login page
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 */
	app.get('/login', (req, res) => {
		res.render(__dirname + VIEW_DIR + 'login.ejs', { message: req.flash('message') })
	})
	/**
	 *
	 * Route post login
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 */
	app.post('/login', passport.authenticate('login', {
		successRedirect: '/home',
		failureRedirect: '/login',
		failureFlash: true
	}))
	/**
	 *
	 * Route get signup
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 */
	app.get('/signup', (req, res) => {
		res.render(__dirname + VIEW_DIR + 'signup.ejs', { message: req.flash('message') })
	})
	/**
	 *
	 * Route post signup
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 */
	app.post('/signup', passport.authenticate('signup', {
		successRedirect: '/home',
		failureRedirect: '/signup',
		failureFlash: true
	}))
	/**
	 *
	 * Route for posts retrieval
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 * @param  {Function} next - Next middleware function
	 */
	app.get('/post', isLoggedIn, (req, res, next) => {
		Post
			.find({userId: req.user._id})
			.sort({timestamp: 'desc'})
			.then((response, err) => {
				if (err) {
					return next('Error fetching posts. Please try again')
				}
				res.setHeader('Content-Type', 'application/json')
				res.send(JSON.stringify(response))
			})
			.catch(function(err) {
				return next('Error fetching posts. Please try again')
			})
	})
	/**
	 *
	 * Route for post insertion
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 * @param  {Function} next - Next middleware function
	 */
	app.post('/post', isLoggedIn, (req, res, next) => {
		const params = req.body
		const journalPost = params.post
		const sentiment = journalPost.split(' ').length * params.happiness
		const newPost = new Post({
			post: journalPost,
			happiness: params.happiness,
			userId: req.user._id,
			sentiment: sentiment
		})
		newPost
			.save()
			.then((response, err) => {
				if (err) {
					console.error('Error inserting post', err)
					return next('Error inserting post. Please try again')
				}
				SocketBus.notifyFamilyHead(req.user.family, sentiment)
				res.setHeader('Content-Type', 'application/json')
				res.send(JSON.stringify(response))
			})
			.catch(function(err) {
				console.error('Error inserting post', err)
				return next('Error inserting post. Please try again')
			})
	})
	/**
	 *
	 * Route get logout
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 */
	app.get('/logout', (req, res) => {
		req.logout();
		res.redirect('/');
	})
	/**
	 *
	 * FOR ADMIN
	 * Route clear
	 *
	 * @param  {Object} req - Request
	 * @param  {Object} res - Response
	 */
	app.get('/clear', isLoggedIn, (req, res) => {
		User.remove({}, () => {
			Post.remove({}, () => {
				res.sendStatus(200)
			})
		})
	})
}
/**
 * Middleware for the routes to check
 * if the user is logged in
 * @param  {Object} req - Request
 * @param  {Object} res - Response
 * @param  {Function} next - Next middleware function
 * @returns {*} Next route
 */
function isLoggedIn(req, res, next) {

	if (req.isAuthenticated()) {
		return next()
	}

	res.redirect('/login')
}

function isUserAuthorized(req, res, next) {
	if (req.user && req.user.familyhead) {
		return next()
	}

	res.redirect('/')
}