const path = require('path')
const express = require('express')
const app = express()
const express_session = require('express-session')
const bodyParser = require('body-parser')
const flash = require('connect-flash')
const mongoose = require('mongoose')
const http = require('http').Server(app)
const io = require('socket.io')(http)

const passport = require('./passport')
const constants = require('./constants')
const socket = require('./socket')
const database = require('./database')
const routes = require('./routes')

const port = process.env.PORT || 8080
const socket_port = process.env.SOCKET_PORT || 3000


//
// Setup database
//
mongoose.Promise = require('bluebird')
mongoose.connect(database.url)

//
// Get socket ready
//
const SocketBus = socket(io)

//
// Get the web server ready
//
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.json())
app.use(flash())

app.use(express_session({
	secret: constants.EXPRESS_SECRET,
	name: constants.COOKIE_NAME,
	resave: true,
	saveUninitialized: true
}))

app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(passport.initialize())
app.use(passport.session())

//
// Get routes ready
//
routes(app, passport, SocketBus)
 
//
// Let the listening begin!
//
app.listen(port, () => {
	console.log('Full house hosted on http://localhost:' + port)
})
http.listen(socket_port)