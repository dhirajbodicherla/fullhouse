const constants = require('./constants')

const SocketBus = {}
SocketBus.io = null;
SocketBus.notifyFamilyHead = function(familyRoom, msg) {
	// sending to all clients in 'family' room, including family head
	this.io.in(familyRoom).emit('message', msg);
}

function onRoomJoinMessage(room) {
	this.join(room);
}

function onRoomLeaveMessage(room) {
	this.leave(room);
}

function onIOConnection(socket) {
	socket.on('joinroom', onRoomJoinMessage)
	socket.on('leaveroom', onRoomLeaveMessage)
}

module.exports = (io) => {
	SocketBus.io = io;
	// IO connection
	io.on('connection', onIOConnection);

	return SocketBus;
}