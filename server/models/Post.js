const mongoose = require('mongoose');
const Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

const postSchema = new Schema({
	id: ObjectId,
	userId: String,
	post: String,
	happiness: Number,
	sentiment: Number,
	timestamp: { type: Date, default: Date.now }
})

const Post = mongoose.model('Post', postSchema);

module.exports = Post