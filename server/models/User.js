const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId

const userSchema = new Schema({
	id: ObjectId,
	username: String,
	password: String,
	family: String,
	familyhead: Number,
	timestamp: { type: Date, default: Date.now }
})


userSchema.index({ family: 1, familyhead: 1}, { unique: true });


userSchema.methods.passwordHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync());
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

const User = mongoose.model('User', userSchema)

module.exports = User