const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const User = require('./models/User')

/**
 * Login Strategy
 * @param  {String} - Strategy name
 * @param  {LocalStrategy} - Strategy instance
 * @param  {Function} - Callback
 * @return {Object} - response
 */
passport.use('login', new LocalStrategy({
	passReqToCallback : true
}, (req, username, password, done) => {
	User.findOne({ username: username }, (err, user) => {
		if (err) { return done(err); }
			if (!user) {
				return done(null, false, req.flash('message', 'Wrong username.'));
			}
			if (!user.validPassword(password)) {
				return done(null, false, req.flash('message', 'Wrong password.'));
			}
			// do not send password back
			delete user.password
			return done(null, user);
		});
	}
));

/**
 * Serialize the user so that
 * login is possible
 */
passport.serializeUser((user, done) => {
	done(null, user._id);
});

/**
 * Deserialize so that user's creds are stored
 */
passport.deserializeUser((id, done) => {
	User.findById(id, (err, user) => {
		done(err, user);
	});
});

/**
 * Signup Strategy
 * @param  {String} - Strategy name
 * @param  {LocalStrategy} - Strategy instance
 * @param  {Function} - Callback
 * @return {Object} - response
 */
passport.use('signup', new LocalStrategy({
	session: false,
	passReqToCallback : true
}, (req, username, password, done) => {
	process.nextTick(() => {
		User.findOne({ username: username }, (err, user) => {
			if (err) { return done(err); }
			if (user) {
				return done(null, false, req.flash('message', 'Email already exists.'));
			} else {

				var newUser = new User();

				newUser.username = username;
				newUser.password = newUser.passwordHash(password);
				newUser.family = req.body.family;
				newUser.familyhead = req.body.familyhead ? req.body.familyhead : 0

				newUser.save((err) => {
					if (err) {
						return done(null, false, req.flash('message', 'Family head already exists.'));
					}
					// do not send password back
					delete newUser.password
					return done(null, newUser);
				});
			}
		});
	});
}));


module.exports = passport