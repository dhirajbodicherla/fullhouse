var OFF = 0;
var WARNING = 1;
var ERROR = 2;
module.exports = {
  'parser': 'babel-eslint',
  'env': {
    'browser': true,
    'amd': true,
    'node': true,
    'jasmine': true,
    'es6': true
  },
  'extends': 'eslint:recommended',
  'parserOptions': {
    'ecmaVersion': 7,
    'ecmaFeatures': {
      'experimentalObjectRestSpread': true,
      'jsx': true
    },
    'sourceType': 'module'
  },
  "ecmaFeatures": {
    "jsx": true,
    "es6": true,
    "classes": true
  },
  'plugins': [
    'react'
  ],
  'rules': {
    'eqeqeq': ERROR,
    'strict': OFF,
    'curly': ERROR,
    'guard-for-in': ERROR,
    'no-unused-vars': WARNING,
    'no-undefined': ERROR,
    'no-bitwise': ERROR,
    'no-trailing-spaces': [ERROR, { 'skipBlankLines': true }],
    'no-mixed-spaces-and-tabs': ERROR,
    'no-multi-spaces': ERROR,
    'require-jsdoc': WARNING,
    'valid-jsdoc': [ERROR, {'requireReturn': false}],
    'no-console': OFF,
    'no-empty': ERROR,
    'no-duplicate-case': ERROR,
    'no-extra-semi': ERROR,
    'semi-spacing': [ERROR, {'before': false, 'after': true}],
    'dot-notation': ERROR,
    'space-before-blocks': ERROR,
    'quotes': [ERROR, 'single'],
    'space-before-function-paren': [ERROR, 'never'],
    'brace-style': [ERROR, '1tbs', { 'allowSingleLine': true }],
    'space-infix-ops': ERROR,
    'array-bracket-spacing': ERROR,
    'space-in-parens': [ERROR, 'never'],
    'comma-dangle': [ERROR, 'never'],
    'comma-style': [ERROR, 'last'],
    'comma-spacing': [ERROR, { 'before': false, 'after': true }],
    'no-sparse-arrays': ERROR,
    'no-unreachable': ERROR,
    'valid-typeof': ERROR,
    'accessor-pairs': WARNING,
    'linebreak-style': [ERROR, 'unix'],
    'keyword-spacing': [ERROR, {'before': true, 'after': true}],
    'react/jsx-uses-react': ERROR,
    'react/jsx-uses-vars': ERROR
  }
};