# Full House

Inspired by the famous TV show Full House from the 90s, this application inspires families to share more and feel more intimate with each other. 

![](http://vignette1.wikia.nocookie.net/fullhouse/images/0/0a/Full_House_Family.png/revision/latest?cb=20091205193852)

## Installation
To install the stable version
You should have installed mongodb and it should be running
```
brew install mongodb
brew services start mongodb
```
If running mongodb as a service runs into errors, it is most likely because of perms. This worked for me 

```
cd /data/db/
sudo chown -R mongod:mongod /data/db/
```
Once mongodb is successfully installed and is running. Run the below commands

```
git clone git@bitbucket.org:dhirajbodicherla/fullhouse.git
cd fullhouse
npm install
npm run start
```

